mockGenerator
=============

Generate a mock folder with mocks for all ts and tsx files within the folder

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/mockGenerator.svg)](https://npmjs.org/package/mockGenerator)
[![Downloads/week](https://img.shields.io/npm/dw/mockGenerator.svg)](https://npmjs.org/package/mockGenerator)
[![License](https://img.shields.io/npm/l/mockGenerator.svg)](https://github.com/https://github.com/0akl3y/Mock Generator/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g mockGenerator
$ mockGenerator COMMAND
running command...
$ mockGenerator (-v|--version|version)
mockGenerator/0.1.0 darwin-x64 node-v12.14.1
$ mockGenerator --help [COMMAND]
USAGE
  $ mockGenerator COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->

<!-- commandsstop -->
